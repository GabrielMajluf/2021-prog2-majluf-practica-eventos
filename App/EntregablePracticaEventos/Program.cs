﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logica;


namespace EntregablePracticaEventos
{
    class Program
    {
        static void Main(string[] args)
        {
            int opcion;
            Principal.ObjPrincipal.ElementoAgregadoEvent += AccionesElementosSuscriptor.ElementoAgregado;
            
            do
            {
                Console.WriteLine("Ingrese el nro de la operacion que quiere realizar. 1 para agregar elemento. 2 para eliminar elemento. " +
                "3 para obtener listado monitores. 4 Obtener listado Computadoras. 5 Listado elementos. 6 salir.");
                opcion = int.Parse(Console.ReadLine());
                switch (opcion)
                {
                    case 1:
                        Console.WriteLine("Ingrese 1 para Monitor, 2 para PC");
                        int elemento = int.Parse(Console.ReadLine());
                        if (elemento == 1)
                        {

                            Console.WriteLine("Ingrese Marca");
                            string marca = Console.ReadLine();
                            Console.WriteLine("Ingrese Modelo");
                            string modelo = Console.ReadLine();
                            Console.WriteLine("Ingrese nro serie");
                            int nroSerie = int.Parse(Console.ReadLine());
                            Console.WriteLine("Ingrese Año fabricacion");
                            int año = int.Parse(Console.ReadLine());
                            Console.WriteLine("Ingrese pulgadas");
                            int? pulgadas = int.Parse(Console.ReadLine());
                            Principal.ObjPrincipal.CargarElemento(modelo, marca, nroSerie, año, pulgadas);
                            
                        }
                        else
                        {

                            Console.WriteLine("Ingrese Marca");
                            string marca = Console.ReadLine();
                            Console.WriteLine("Ingrese Modelo");
                            string modelo = Console.ReadLine();
                            Console.WriteLine("Ingrese nro serie");
                            int nroSerie = int.Parse(Console.ReadLine());
                            Console.WriteLine("Ingrese descProcesador");
                            string descProcesador = Console.ReadLine();
                            Console.WriteLine("Ingrese CantidadRAM");
                            int cantidadRam = int.Parse(Console.ReadLine());
                            Console.WriteLine("Ingrese NombreFabricante");
                            string NombreFabricante = Console.ReadLine();
                            Principal.ObjPrincipal.CargarElemento(modelo, marca, nroSerie, descProcesador, cantidadRam, NombreFabricante);
                        }
                        break;
                    case 2:

                        break;
                    default:
                        break;
                }
            } while (opcion < 5);
        }
    }
    public class AccionesElementosSuscriptor
    {

        public static void ElementoAgregado(object sender, AgregarElemento args)
        {
            if (args.elemento is Monitor monitor)
            {
                Console.WriteLine($"El monitor fue agregado. Detalle: {monitor.Identificador}");
            }
            else
            {
                Console.WriteLine($"La computadora fue agregada. Detalle: {args.elemento.Identificador}");
            }
        }
    }
}
