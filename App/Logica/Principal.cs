﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public sealed class Principal
    {
        public EventHandler<AgregarElemento> ElementoAgregadoEvent;

        private static Principal principal = new Principal();

        private Principal()
        {

        }

        public static Principal ObjPrincipal
        {
            get
            {
                return principal;
            }

        }

        public List<ElementoComputacion> ListaElementosComputacion = new List<ElementoComputacion>();

        public void CargarElemento(string pModelo, string pMarca, int pNumeroSerie, int pAñoFabricacion, int? pPulgadas)
        {
            Monitor monitor = new Monitor();

            monitor.Modelo = pModelo;
            monitor.Marca = pMarca;
            monitor.NumeroSerie = pNumeroSerie;
            monitor.AñoFabricacion = pAñoFabricacion;
            monitor.Pulgada = pPulgadas;
            

            ListaElementosComputacion.Add(monitor);
            this.ElementoAgregadoEvent(this, new AgregarElemento() { elemento = monitor });
        }
        public void CargarElemento(string pModelo, string pMarca, int pNumeroSerie, string pDescProcesador, int pCantRAM, string pNombreFabr)
        {
            Computadora computadora = new Computadora();

            computadora.Modelo = pModelo;
            computadora.Marca = pMarca;
            computadora.NumeroSerie = pNumeroSerie;
            computadora.DescripcionProcesador = pDescProcesador;
            //computadora.CantRAM = pCantRAM;   VER COMO HACER ESTO
            computadora.NombreFabricante = pNombreFabr;

            ListaElementosComputacion.Add(computadora);
            this.ElementoAgregadoEvent(this, new AgregarElemento() { elemento = computadora });
        }

        public void ValidaryEliminarElementos(string pIdentificador)
        {
            if ((ListaElementosComputacion.Find(x => x.Identificador == pIdentificador) != null))
            {
                ListaElementosComputacion.Remove((ListaElementosComputacion.Find(x => x.Identificador == pIdentificador)));
            }

        }

        public List<string> ObtenerDecripcionMonitores()
        {
            List<string> ListaMonitores = new List<string>();
            foreach (var monitor in ListaElementosComputacion)
            {
                if (monitor is Monitor monitor1)
                {
                    string str = $"MONITOR - {monitor1.Marca} - {monitor1.Modelo} - {monitor1.Pulgada}";
                }
            }
            return ListaMonitores;
        }
        public List<string> ObtenerDecripcionComputadoras()
        {
            List<string> ListaComputadoras = new List<string>();
            foreach (var computadora in ListaElementosComputacion)
            {
                if (computadora is Computadora computadora1)
                {
                    string str = $"COMPUTADORA - {computadora1.Marca} - {computadora1.Modelo} - {computadora1.DescripcionProcesador} - {computadora1.NombreFabricante}";
                }
            }
            return ListaComputadoras;
        }

        public List<string> ObtenerListadoElementos()
        {
            List<string> ListadoElementos = new List<string>();
            ListadoElementos.Add(ObtenerDecripcionMonitores().ToString());
            ListadoElementos.Add(ObtenerDecripcionComputadoras().ToString());  
            ListadoElementos.Sort();
            return ListadoElementos;
        }
    }
}