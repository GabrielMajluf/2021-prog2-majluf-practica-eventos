﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Computadora : ElementoComputacion
    {
        public string DescripcionProcesador { get; set; }
        public enum CantRAM { DosGB, CuatroGB, OchoGB, DieciseisGB }
        public string NombreFabricante { get; set; }
    }
}
