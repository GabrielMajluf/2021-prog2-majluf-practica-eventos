﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Monitor : ElementoComputacion
    {
        public int AñoFabricacion { get; set; }
        public int? Pulgada { get; set; }
        
        public bool Condicion 
        { get
            {
                if (AñoFabricacion == DateTime.Today.Year)
                {
                    return true;
                }
                return false;
            }       
        }
    }
}
